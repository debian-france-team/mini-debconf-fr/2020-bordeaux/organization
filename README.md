Organisation de la Mini-DebConf 2020 à Bordeaux
===============================================

Date de la Mini-DebConf Bordeaux : **10 et 11 Octobre 2020**

----------------------------------------

## Réunions

----------------------------------------

Prochaine réunion :
+ Mardi 30 Juin 2020 à 21h (OFTC, canal #mini-debconf-fr)
  * [Sujets](reunion-2020-06-30.md)

----------------------------------------

Historiques (logs et CR) :
+ [Réunion 2020/05/26 - Logs](http://meetbot.debian.net/mini-debconf-fr/2020/mini-debconf-fr.2020-05-26-18.59.log.html)
+ [Réunion 2020/04/28 - Sujets et CR](reunion-2020-04-28.md)
+ [Réunion 2020/04/28 - Logs](http://meetbot.debian.net/mini-debconf-fr/2020/mini-debconf-fr.2020-04-28-18.59.log.html)
+ [Réunion 2020/02/25 - Logs](http://meetbot.debian.net/mini-debconf-fr/2020/mini-debconf-fr.2020-02-25-19.06.log.html)

----------------------------------------

## Organisation

----------------------------------------

+ [Liste des volontaires](liste_des_volontaires.md)
+ [Liste des postes et missions](postes_et_missions.md)
+ [Liste des speakers](liste_des_speakers.md)

----------------------------------------

+ [Sponsors](sponsoring.md)
+ [Goodies](goodies.md)
