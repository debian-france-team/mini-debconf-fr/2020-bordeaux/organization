Postes et missions
==================

Postes pourvus :
- Chargé de comm' (réseaux sociaux et autre) : Alban (Voir avec Quentin)
- Site et formulaire d'inscription : Jeremy et Gregory
- Sponsoring : Alban
- Chargé des présentations: Dylan
- Chargé technique (wifi / mobilier): Lilian


Postes à pourvoir :
- Chargé de la restauration
- Chargé des bénévoles
- Chargé logement (recherche et propositions de logements à proximité)
- Chargé serveur SKS (PGP pour signature des clés)

À faire :
- Logo : Voir avec Juliette si volontaire pour nous aider
- Tee-shirts : Voir une fois logo fait (Alban volontaire)
- Audio/Vidéo : Voir avec l'équipe Debian Vidéo une fois la date définie

