Goodies
=======

Propositions pour les goodies :
+ Tee-shirt pour chaque participant (avec ou sans sponsors, à voir)
+ Tee-shirt « organisation »

+ Porte gobelet tour de cou
  - (0.98) http://www.monproduitdecom.com/evenementiel/gobelet-730-noir.html

+ Mug en plastique (avec ou sans sponsors, à voir)
  - (0.57) 60cl http://www.monproduitdecom.com/evenementiel/gobelet-728.html
  - (0.24) 50cl http://www.monproduitdecom.com/evenementiel/gobelet-729.html

+ Tour de coup avec cordon « Debian »
  - Orga - Rouge - « Debian Staff »
  - Visiteurs - Noir - « LOGO Debian »
  - Matos :
    - (1.73) http://www.monproduitdecom.com/evenementiel/tours_de_cou-1111-vert_anis.html
    - (2.49) http://www.monproduitdecom.com/evenementiel/tours_de_cou-1030.html
