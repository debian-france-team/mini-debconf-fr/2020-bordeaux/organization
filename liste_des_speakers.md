Liste des speakers
==================

## Confirmé

| Nom                            | Titre / Sujet                            | Durée souhaitée      |
| ------------------------------ | ---------------------------------------- | -------------------- |

## Peut-être

| Nom                            | Titre / Sujet                            | Durée souhaitée      |
| ------------------------------ | ---------------------------------------- | -------------------- |

