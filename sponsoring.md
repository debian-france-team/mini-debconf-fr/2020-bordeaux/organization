Sponsoring
==========

Pour le « sponsoring », je vous propose plusieurs niveaux, par exemple :
- Bronze : 200€ => Mention sur le site avec logo
- Argent : 500€ => Précédent + Logo sur les flyers
- Or : 1000€ et + => Précédent + Logo sur le dos du tee-shirt

Ce sont des exemples à travailler suivant le budget et les idées.
